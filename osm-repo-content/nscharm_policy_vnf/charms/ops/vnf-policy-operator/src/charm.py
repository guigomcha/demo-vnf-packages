#!/usr/bin/env python3
# Copyright 2020 David Garcia
# See LICENSE file for licensing details.

from ops.main import main
from ops.charm import CharmBase
from ops.model import ActiveStatus


class VnfPolicyCharm(CharmBase):
    def __init__(self, *args):
        super().__init__(*args)

        # Charm events
        self.framework.observe(self.on.install, self._on_install)
        self.framework.observe(self.on.set_policy_action, self._on_set_policy_action)

    def _on_install(self, _):
        self.unit.status = ActiveStatus()

    def _on_set_policy_action(self, event):
        """Set policy action."""
        event.set_results({"updated": True})


if __name__ == "__main__":
    main(VnfPolicyCharm)
