# Native K8S Scale Charm

## Upload packages
```bash
osm upload-package native_k8s_scale_charm_vnf
osm upload-package native_k8s_scale_charm_ns
```

## Deploy the service
```bash
osm ns-create --ns_name native_k8s_scale --nsd_name native_k8s_scale_charm-ns --vim_account <vim-account> --config '{vld: [ {name: mgmtnet, vim-network-name: <vim-network-name>} ] }'
```

## Scale the service

### Scale-out
```bash
osm vnf-scale native_k8s_scale native_k8s_scale_charm-vnf --scaling-group scale-kdu --scale-out
```
### Scale-in
```bash
osm vnf-scale native_k8s_scale native_k8s_scale_charm-vnf --scaling-group scale-kdu --scale-in
```
