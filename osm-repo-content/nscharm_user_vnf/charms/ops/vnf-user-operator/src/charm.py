#!/usr/bin/env python3
# Copyright 2020 David Garcia
# See LICENSE file for licensing details.

import random

from ops.main import main
from ops.charm import CharmBase
from ops.model import ActiveStatus


class VnfUserCharm(CharmBase):
    def __init__(self, *args):
        super().__init__(*args)

        # Charm events
        self.framework.observe(self.on.install, self._on_install)
        self.framework.observe(self.on.add_user_action, self._on_add_user_action)

    def _on_install(self, _):
        self.unit.status = ActiveStatus()

    def _on_add_user_action(self, event):
        """Add user action."""
        event.set_results({"user-id": random.randint(1, 100)})


if __name__ == "__main__":
    main(VnfUserCharm)
