# README

## Cloning the repo

You need to clone the repo with the option "--recursive" to fetch all the dependent git modules:

```bash
# Clone with SSH
git clone --recursive ssh://git@osm.etsi.org:29419/vnf-onboarding/osm-packages.git
# Clone with HTTPS
git clone --recursive https://osm.etsi.org/gitlab/vnf-onboarding/osm-packages.git
```

## Structure of the repo

This repo contains OSM packages used for testing and for the hackfest.

There is typically a folder for each VNF under test and NS to run the test, for instance:

- `hackfest_basic_vnf`: the VNF package to run the hackfest basic test
- `hackfest_basic_ns`: the NS package to run the hackfest basic test

Please use the following conventions:

- VNF folders should end with "_vnf" or "_knf"
- NS folders should end with "_ns"

Charm sources must be stored in the folder of the package, in `charms/layers` folder. If relations are in place, the charm interfaces must be stored in `charms/interfaces` folder.

Thanks to the new way for building and generating packages, charms sources can be placed directly in the package folder, under `charms/layers/` folder, thus simplifying the structure. When building the package, the charm will be built and only the binaries will be placed in the tar.gz file.

## Working with packages

Osmclient has been improved so that all utilities required to validate and build the packages have been
added. Now the single `osm` command line client is able to do everything you need to work with VNF and
NS packages.

### Uploading a package from a folder

This is by far the simplest way of working. The following command will validate the package,
build the charm from source if it exists under `PKGFOLDER/charms/layers`, build the tar.gz,
and onboard/upload it to OSM.

```bash
osm nfpkg-create PKGFOLDER
```

For NS packages, the same applies.

```bash
osm nspkg-create PKGFOLDER
```

Full help for both commands is show below:

```bash
$ osm nfpkg-create --help
Usage: osm nfpkg-create [OPTIONS] FILENAME

  onboards a new NFpkg (alias of nfpkg-create)

  FILENAME: NF Package tar.gz file, NF Descriptor YAML file or NF Package folder
            If FILENAME is a file (NF Package tar.gz or NF Descriptor YAML), it is onboarded.
            If FILENAME is an NF Package folder, it is built and then onboarded.

Options:
  --overwrite TEXT     Deprecated. Use override
  --override TEXT      overrides fields in descriptor, format: "key1.key2...=value[;key3...=value;...]"
  --skip-charm-build   The charm will not be compiled, it is assumed to already exist
  --override-epa       adds guest-epa parameters to all VDU
  --override-nonepa    removes all guest-epa parameters from all VDU
  --override-paravirt  overrides all VDU interfaces to PARAVIRT
  -h, --help           Show this message and exit.
```

```bash
$ osm nspkg-create --help
Usage: osm nspkg-create [OPTIONS] FILENAME

  onboards a new NSpkg

  FILENAME: NF Package tar.gz file, NF Descriptor YAML file or NF Package folder
            If FILENAME is a file (NF Package tar.gz or NF Descriptor YAML), it is onboarded.
            If FILENAME is an NF Package folder, it is built and then onboarded.

Options:
  --overwrite TEXT    Deprecated. Use override
  --override TEXT     overrides fields in descriptor, format: "key1.key2...=value[;key3...=value;...]"
  --skip-charm-build  The charm will not be compiled, it is assumed to already exist
  -h, --help          Show this message and exit.
```

### Validating a descriptor

The following command will look for all the descriptor files in a folder and validate them

```bash
osm package-validate PKGFOLDER
```

Some relevant options are shown below:

```bash
$ osm package-validate --help
Usage: osm package-validate [OPTIONS] [BASE_DIRECTORY]

  Validate descriptors given a base directory.

  BASE_DIRECTORY: Stub folder for NS, VNF or NST package.

Options:
  --recursive / --no-recursive  The activated recursive option will validate the yaml files within the indicated directory and in its subdirectories
  -h, --help                    Show this message and exit.
```

### Building a package

The following command will validate and build the tar.gz file. It will als

```bash
osm package-build PKGFOLDER
```

Some relevant options are shown below:

```bash
$ osm package-build --help
Usage: osm package-build [OPTIONS] PACKAGE_FOLDER

  Build the package NS, VNF given the package_folder.

  PACKAGE_FOLDER: Folder of the NS, VNF or NST to be packaged

Options:
  --skip-validation   skip package validation
  --skip-charm-build  the charm will not be compiled, it is assumed to already exist
  -h, --help          Show this message and exit.
```

### Creating a package structure

The best way to work is by copying a previous one that is up-to-date. But you can also create
a first skeleton using this command:

```bash
Usage: osm package-create [OPTIONS] PACKAGE_TYPE PACKAGE_NAME

  Creates an OSM NS, VNF, NST package

  PACKAGE_TYPE: Package to be created: NS, VNF or NST.
  PACKAGE_NAME: Name of the package to create the folder with the content.

Options:
  --base-directory TEXT       (NS/VNF/NST) Set the location for package creation. Default: "."
  --image TEXT                (VNF) Set the name of the vdu image. Default "image-name"
  --vdus INTEGER              (VNF) Set the number of vdus in a VNF. Default 1
  --vcpu INTEGER              (VNF) Set the number of virtual CPUs in a vdu. Default 1
  --memory INTEGER            (VNF) Set the memory size (MB) of the vdu. Default 1024
  --storage INTEGER           (VNF) Set the disk size (GB) of the vdu. Default 10
  --interfaces INTEGER        (VNF) Set the number of additional interfaces apart from the management interface. Default 0
  --vendor TEXT               (NS/VNF) Set the descriptor vendor. Default "OSM"
  --override                  (NS/VNF/NST) Flag for overriding the package if exists.
  --detailed                  (NS/VNF/NST) Flag for generating descriptor .yaml with all possible commented options
  --netslice-subnets INTEGER  (NST) Number of netslice subnets. Default 1
  --netslice-vlds INTEGER     (NST) Number of netslice vlds. Default 1
  -h, --help                  Show this message and exit.
```



