#!/bin/bash
set -x
declare -a VNFPKG_LIST=(
    "affinity_basic_vnf"
    "ubuntu_cloudinit_vnf"
    "charm-packages/ha_proxy_charm_vnf"
    "charm-packages/k8s_proxy_charm_vnf"
    "charm-packages/native_charm_vnf"
    "charm-packages/native_k8s_charm_vnf"
    "charm-packages/native_k8s_scale_charm_vnf"
    "charm-packages/native_manual_scale_charm_vnf"
    "charm-packages/nopasswd_k8s_proxy_charm_vnf"
    "charm-packages/nopasswd_proxy_charm_vnf"
    "charm-packages/ns_relations_provides_vnf"
    "charm-packages/ns_relations_requires_vnf"
    "charm-packages/vnf_relations_vnf"
    "cirros_alarm_vnf"
    "epa_1vm_passthrough_vnf"
    "epa_1vm_sriov_vnf"
    "epa_quota_vnf"
    "hackfest_basic_metrics_vnf"
    "hackfest_basic_sriov_vnf"
    "hackfest_basic_vnf"
    "hackfest_cloudinit_vnf"
    "hackfest_multivdu_vnf"
    "ipprofile_2vm_vnf"
    "nscharm_policy_vnf"
    "nscharm_user_vnf"
    "openldap_knf"
    "openldap_primitives_knf"
    "openldap_scale_knf"
    "simple_2vm_vnf"
    "slice_basic_middle_vnf"
    "slice_basic_vnf"
    "snmp_ee_vnf"
    "ubuntu_4ifaces_vnf"
)
declare -a NSPKG_LIST=(
    "affinity_basic_ns"
    "charm-packages/ha_proxy_charm_ns"
    "charm-packages/k8s_proxy_charm_ns"
    "charm-packages/native_charm_ns"
    "charm-packages/native_k8s_charm_ns"
    "charm-packages/native_k8s_scale_charm_ns"
    "charm-packages/native_manual_scale_charm_ns"
    "charm-packages/nopasswd_k8s_proxy_charm_ns"
    "charm-packages/nopasswd_proxy_charm_ns"
    "charm-packages/ns_relations_ns"
    "charm-packages/vnf_relations_ns"
    "cirros_alarm_ns"
    "epa_1vm_passthrough_ns"
    "epa_1vm_sriov_ns"
    "epa_quota_ns"
    "hackfest_basic_metrics_ns"
    "hackfest_basic_ns"
    "hackfest_basic_sriov_ns"
    "hackfest_cloudinit_ns"
    "hackfest_multivdu_ns"
    "ipprofile_2vm_ns"
    "nscharm_ns"
    "openldap_ns"
    "simple_2vm_ns"
    "slice_basic_middle_ns"
    "slice_basic_ns"
    "snmp_ee_ns"
    "ubuntu_4ifaces_ns"
    "ubuntu_cloudinit_ns"
)
cd osm-repo-content
# CHECK THAT UNKNOWN Charts have not been added
RAW_COUNT=$(find . -maxdepth 1 -type d | wc -l)
# Subtract folder ., .git and charm
COUNT=$(expr $RAW_COUNT - 3)
RAW_CHARM_COUNT=$(find charm-packages/ -maxdepth 1 -type d | wc -l)
# Charm-packages also has the default folder .
COUNT=$(expr $COUNT + $RAW_CHARM_COUNT - 1)
VNFPKG_KNOWN_COUNT="${#VNFPKG_LIST[@]}"
NSPKG_KNOWN_COUNT="${#NSPKG_LIST[@]}"
KNOWN_COUNT=$(expr $VNFPKG_KNOWN_COUNT + $NSPKG_KNOWN_COUNT)
echo "$KNOWN_COUNT vs $COUNT"
if [ "$KNOWN_COUNT" -eq "$COUNT" ]; then
    echo "All expected packages are found"
else
    echo "Error: list of VNFPKGs or NSPKGs not updated"
    exit 1
fi

#for d in *ns; do
for d in ${VNFPKG_LIST[@]}; do
    echo "osm package-build $d"
    osm package-build $d
done
for d in ${NSPKG_LIST[@]}; do
    echo "osm package-build $d"
    osm package-build $d
done

