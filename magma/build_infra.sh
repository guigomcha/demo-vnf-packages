# Add Helm repo for Magma Orc8r
osm repo-add --type helm-chart --description "Repository for Facebook Magma helm Chart" magma http://osm-download.etsi.org/ftp/helm/magma/

# Add first VIM
osm vim-create --name etsi-openstack --user hackfest --password hackfest20$ --auth_url http://172.21.247.1:5000/v3 --tenant hackfest --account_type openstack --config='{management_network_name: osm-ext, dataplane_physical_net: physnet2, microversion: 2.32}'

# Add first PDU
VIMID=`osm vim-list | grep "etsi-openstack " | awk '{ print $4 }'`
sed -i "s/vim_accounts: .*/vim_accounts: [ $VIMID ]/" pdu.yaml
osm pdu-create --descriptor_file pdu.yaml

# Add second VIM
osm vim-create --name etsi-openstack-lowcost --user osm_mdl --password osmmdl20$ --auth_url http://172.21.7.5:5000/v3 --tenant osm_mdl --account_type openstack --config='{management_network_name: management}'

# Add second PDU
VIMID=`osm vim-list | grep "etsi-openstack-lowcost" | awk '{ print $4 }'`
sed -i "s/vim_accounts: .*/vim_accounts: [ $VIMID ]/" pdu.yaml
osm pdu-create --descriptor_file pdu.yaml

# Add K8sCluster to first VIM
osm k8scluster-add --creds ~/kube.yaml --version '1.15' --vim etsi-openstack --description "My K8s cluster" --k8s-nets '{"net1": "osm-ext"}' etsi-k8scluster

# Add SDN Controller
#osm sdnc-create --name onos01 --type onos_vpls --url http://172.21.248.19:8181 --user karaf --password karaf 
#osm vim-update whitecloud_highcost_sriov --sdn_controller onos02 --sdn_port_mapping sdn_port_mapping.yaml
