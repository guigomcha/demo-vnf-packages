tar -cvzf hackfest_squid_cnf_ns.tar.gz hackfest_squid_cnf_ns/
tar -cvzf hackfest_squid_cnf.tar.gz hackfest_squid_cnf/

osm nsd-delete squid-cnf-ns 
osm vnfd-delete squid-vnf

osm vnfd-create hackfest_squid_cnf.tar.gz
osm nsd-create hackfest_squid_cnf_ns.tar.gz

osm ns-create --ns_name webcache --nsd_name squid-cnf-ns --vim_account etsi-openstack --config '{vld: [ {name: mgmtnet, vim-network-name: osm-ext} ] }'