osm nst-delete magma_slice_hackfest_nst
osm nsd-delete hackfest_magma-agw-enb_nsd
osm nsd-delete fb_magma_ns
osm vnfd-delete hackfest_magma-agw-enb_vnfd
osm vnfd-delete hackfest_gateway_vnfd
osm vnfd-delete fb_magma_knf

osm nfpkg-create hackfest_magma-agw-enb_vnfd
osm nfpkg-create hackfest_gateway_vnfd
osm nspkg-create hackfest_magma-agw-enb_nsd
osm nfpkg-create fb_magma_knf
osm nspkg-create fb_magma_ns

osm netslice-template-create magma_slice.yaml

osm nsi-create --nsi_name magma_slice --nst_name magma_slice_hackfest_nst \
--config_file params.yaml --ssh_keys ~/.ssh/id_rsa.pub --vim_account etsi-openstack

sleep 15
osm ns-list | grep "\<slice_hackfest_nsd_epc\>" | awk '{print $4}' | xargs -l1 juju switch
juju model-config enable-os-refresh-update=false enable-os-upgrade=false
juju model-config apt-mirror=http://fr.archive.ubuntu.com/ubuntu/
