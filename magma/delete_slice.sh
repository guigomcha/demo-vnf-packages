#!/bin/bash
osm nsi-delete magma_slice
osm ns-delete magma_slice.slice_hackfest_nsd_epc
osm ns-delete magma_slice.slice_hackfest_nsd_epcmgmt
PROJECT=$(osm project-list | grep $OSM_PROJECT | awk '{print $4}')
echo "To ensure complete deletion, run this in your K8s cluster: kubectl delete namespace $PROJECT"