#!/usr/bin/env bash
set -o errexit
set -o nounset
#set -o xtrace

if [ ! -x "$(command -v charmcraft)" ]; then
  echo >&2 "You must have the charmcraft command to use this script."
  exit 1
fi

pushd .
rm -rf ./charms/powerdns-operator
cd ./ops/powerdns-operator
charmcraft build -v
mkdir -p ../../charms
mv build ../../charms/powerdns-operator
popd

echo "Done"

