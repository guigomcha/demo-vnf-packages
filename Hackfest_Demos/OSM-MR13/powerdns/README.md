# PowerDNS NS

Descriptors that installs a PowerDNS chart from [helm-chart repo](https://gatici.github.io/helm-repo/).

There is one VNF (powerdns_knf) with only one KDU.

There is one NS that connects the VNF to a mgmt network

## Download Packages

```bash
git clone --recurse-submodules -j8 https://osm.etsi.org/gitlab/vnf-onboarding/osm-packages.git
```

## Create the VIM Account

```bash
# This is dummy vim account
export VIM_ACCOUNT=openstack
osm vim-create --name $VIM_ACCOUNT \
               --account_type dummy \
               --user dummy \
                --password dummy \
                --auth_url "http://dummy" \
                --tenant dummy
```

## Add K8s Cluster

```bash
# kubeconfig.yaml exists in the HOME directory
export K8S_NET=osm-ext  # osm-ext
export K8S_CLUSTER=hackfest
osm k8scluster-add --creds ~/kubeconfig.yaml \
                     --vim openstack \
                     --k8s-nets "{k8s_net: $K8S_NET}" \
                     --version 1.24  \
                     $K8S_CLUSTER
```

## Add Helm Repository

```bash
osm repo-add --type helm-chart --description "Repository for Powerdns helm Chart" osm-helm https://gatici.github.io/helm-repo/
```

## Build the charm

```bash
# Install charmcraft
sudo snap install charmcraft --classic
pushd osm-packages/Hackfest_Demos/OSM-MR13/powerdns/powerdns_knf/charms/ops/powerdns-operator
# Pack charm
charmcraft pack
# Copy charm under VNFD/charms folder
cp powerdns-operator_ubuntu-20.04-amd64.charm  osm-packages/Hackfest_Demos/OSM-MR13/powerdns/powerdns_knf/charms/
popd
```

## Onboarding and instantiation

```bash
export VNF_NAME=powerdns
export KDU_NAME=powerdns
# Define the NS name
export PDNS_NS_NAME=powerdns_ns
```

```bash
pushd osm-packages/Hackfest_Demos/OSM-MR13/powerdns/
osm nfpkg-create powerdns_knf
osm nspkg-create powerdns_ns
popd
export PDNS_NS_ID=`osm ns-create --ns_name $PDNS_NS_NAME --nsd_name powerdns_ns --vim_account openstack --config "{vld: [ {name: mgmtnet, vim-network-name: $K8S_NET}]}"`
# Check NS status
osm ns-list
osm ns-show $PDNS_NS_ID
```

## Test Day2 Actions: add-zone, add-domain

```bash
# Add Zone Action
# Define zone such as "example.org."
ZONE=osm.magma.com. 
ADD_ZONE_OP_ID=`osm ns-action --action_name add-zone --vnf_name $VNF_NAME --kdu_name $KDU_NAME --params "{"zone_name": $ZONE}" $PDNS_NS_NAME`
# Check operation status
osm ns-op-show $ADD_ZONE_OP_ID
# Add Domain Action
# Define domain such as "test."
# Use the domain names and ip addresses which are required for Magma Orchestrator
DOMAIN=<domain> 
# Define ip such as "192.168.2.32"
IP=<ip>
ADD_DOMAIN_OP_ID=`osm ns-action --action_name add-domain --vnf_name $VNF_NAME --kdu_name $KDU_NAME  --params "{'zone_name': $ZONE, 'subdomain': $DOMAIN, 'ip': $IP}" $PDNS_NS_NAME`
# Check operation status
osm ns-op-show $ADD_DOMAIN_OP_ID
# Get the list of all operations
osm ns-op-list $PDNS_NS_ID
```

## Testing PowerDNS server

```bash
VNF_ID=`osm vnf-list --ns $PDNS_NS_NAME | grep powerdns | awk '{print $2}'`
export DNS_IP=`osm vnf-show $VNF_ID --literal | yq -e '.kdur[0].services[] | select(.name | contains("udp")) | .external_ip' | tr -d ' '-`
# Setting DNS Server in your machine
echo "nameserver ${DNS_IP}" | tee -a  /etc/resolv.conf
RECORD=<domain><zone> 
# Sample record: "test.example.org"
dig @${DNS_IP} $RECORD
```

## Test Day2 Actions: delete-domain, delete-zone

```bash
# Delete Domain
DEL_DOM_OP_ID=`osm ns-action --action_name delete-domain --vnf_name $VNF_NAME --kdu_name $KDU_NAME  --params "{'zone_name': $ZONE, 'subdomain': $DOMAIN}" $PDNS_NS_NAME`
osm ns-op-show $DEL_DOM_OP_ID
dig @${DNS_IP} ${RECORD}
# Delete Zone
DEL_ZONE_OP_ID=`osm ns-action --action_name delete-zone --vnf_name $KDU_NAME --kdu_name $KDU_NAME  --params "{'zone_name': $ZONE}" $PDNS_NS_NAME`
osm ns-op-show $DEL_ZONE_OP_ID
```

## Upgrade Operation: Scale Out

```bash
SCALE_OUT_OP_ID=`osm ns-action --action_name upgrade --vnf_name $VNF_NAME  --kdu_name $KDU_NAME --params "{'replicaCount':'3',}" $PDNS_NS_NAME`
osm ns-op-show $PDNS_NS_NAME --literal | yq .operationState
osm vnf-show $VNF_ID --kdu $KDU_NAME | yq .config.replicaCount
```

## Rollback Operation: Scale In

```bash
SCALE_IN_OP_ID=`osm ns-action --action_name rollback --vnf_name $VNF_NAME --kdu_name $KDU_NAME $PDNS_NS_NAME`
osm ns-op-show $SCALE_IN_OP_ID --literal | yq .operationState
osm vnf-show $VNF_ID --kdu $KDU_NAME | yq .config.replicaCount 
```
