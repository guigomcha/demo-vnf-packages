#!/bin/bash

echo "========================================================================"
echo "Enabling Debug Mode for LCM"
echo "========================================================================"

cat << 'EOF'

juju config lcm debug-mode=true
juju run-action lcm/0 get-debug-mode-information --wait

Once the debugger has started, go to n2vc/n2vc_juju_conn.py #L 1051

EOF 
