#!/bin/bash

echo "========================================================================"
echo "Here are some of the actions you can run"
echo "========================================================================"

cat << 'EOF'

osm ns-action firewall --vnf_name VYOS-PNF --action_name add-port-forward --params "{ruleNumber: '10', sourcePort: '5022', destinationAddress: '192.168.239.NN', destinationPort: '22'}"
osm ns-action firewall --vnf_name VYOS-PNF --action_name remove-port-forward --params '{ruleNumber: "10"}'

EOF 
