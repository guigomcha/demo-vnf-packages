#!/bin/bash 

HFID=$(echo $HOSTNAME | cut -f2 -d-)
IP=$(expr 100 + $HFID)
cd ~/osm-packages
if [ ! -d hackfest_firewall_pnf ] ; then
    echo "It does not look like we are in the osm-packages directory, exiting"
    exit 1
fi

echo "========================================================================"
echo "Cleaning out any prior versions of the descriptors from OSM"
echo "========================================================================"
osm nsd-delete hackfest_firewall_pnf_ns | grep -v "not found"
osm vnfd-delete hackfest_firewall_pnf | grep -v "not found"
osm pdu-delete router01 | grep -v "not found"

echo "========================================================================"
echo "Uploading packages"
echo "========================================================================"
osm upload-package hackfest_firewall_pnf
osm upload-package hackfest_firewall_pnf_ns

echo "========================================================================"
echo "Registering PDU 172.21.19.${IP} with OSM"
echo "========================================================================"

cat << EOF > firewall-pdu.yaml
name: router01
description: VyOS Router
type: gateway
shared: false
interfaces:
 -  name: gateway_public
    ip-address: 172.21.19.${IP}
    mgmt: true
    vim-network-name: osm-ext
 -  name: vnf_internal
    ip-address: 192.168.239.250
    mgmt: false
    vim-network-name: private
EOF

osm pdu-create --descriptor_file firewall-pdu.yaml --vim_account openstack
echo "========================================================================"
echo "Done"
echo "========================================================================"
