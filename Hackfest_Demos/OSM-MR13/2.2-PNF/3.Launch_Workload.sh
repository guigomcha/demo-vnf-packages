#!/bin/bash

echo "========================================================================"
echo "Launching a workload behind the firewall"
echo "========================================================================"

openstack server create --image=ubuntu20.04 --flavor=m1.medium \
          --network private --key-name hackfest workload
