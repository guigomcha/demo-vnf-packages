#!/bin/bash

echo "========================================================================"
echo "Waiting for deployment to finish"
echo "========================================================================"

WAITING=1
while [ $WAITING -eq 1 ] ; do
    osm ns-list | grep my_first_ns | grep READY
    WAITING=$?
done

echo "========================================================================"
echo "Getting IP Address of VNF"
echo "========================================================================"

IP_ADDRESS=$(osm vnf-list | grep my_first_vnf | awk '{print $14}')

echo "Your VNF is reachable at ${IP_ADDRESS}"
echo "To log in, use"
echo "  ssh ubuntu@${IP_ADDRESS}"

echo "========================================================================"
echo "Done"
echo "========================================================================"
