#!/bin/bash

if [ ! -d my_first_ns ] ; then
    echo "It does not look like we are in the osm-packages directory, exiting"
    exit 1
fi


echo "========================================================================"
echo "Cleaning out any prior versions of the descriptors from OSM"
echo "========================================================================"
osm nsd-delete my_first_ns | grep -v "not found"
osm vnfd-delete my_first_vnf | grep -v "not found"

echo "========================================================================"
echo "Uploading packages"
echo "========================================================================"
osm upload-package my_first_vnf
osm upload-package my_first_ns
echo "========================================================================"
echo "Done"
echo "========================================================================"

