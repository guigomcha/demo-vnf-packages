#!/bin/bash

echo "========================================================================"
echo "Try out your new action.  Make sure you are logged into the VNF VM"
echo "========================================================================"

cat << 'EOF'

osm ns-action --vnf_name my_first_vnf --action_name announce --params "{message: hi}" my_first_ns

EOF 
