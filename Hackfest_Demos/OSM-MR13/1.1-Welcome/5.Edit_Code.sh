#!/bin/bash
cat << EOF
==============================================================================================
File: my_first_vnf/charms/my-first-charm/src/charm.py

Around line 87, change _announce to read as follows:

    def _announce(self, event):
        self.unit.status = MaintenanceStatus("Announce")
        message = event.params["message"]
        shell(f"wall \"{message}\"")
        self.unit.status = self._get_current_status()

==============================================================================================
Launching VSCode, alt-tab to get back to this window :)
EOF
sleep 4
code -g ~/osm-packages/Hackfest_Demos/OSM-MR13/1.1-Welcome/my_first_vnf/charms/my-first-charm/src/charm.py:87
