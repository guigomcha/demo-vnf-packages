#!/bin/bash

echo "========================================================================"
echo "Here are some of the actions you can run"
echo "========================================================================"

cat << EOF

osm ns-action my_first_ns --wait --vnf_name my_first_vnf --action_name reboot
osm ns-action my_first_ns --wait --vnf_name my_first_vnf --action_name cancel-reboot

osm ns-action my_first_ns --wait --vnf_name my_first_vnf --action_name add-package --params '{package: traceroute}'
osm ns-action my_first_ns --wait --vnf_name my_first_vnf --action_name remove-package --params '{package: traceroute}'

osm ns-action my_first_ns --wait --vnf_name my_first_vnf --action_name update-system

EOF