#!/bin/bash

echo "========================================================================"
echo "Performing software update"
echo "========================================================================"

NS_ID=$(osm ns-list | grep my_first_ns | awk '{print $4}')
VNF_ID=$(osm vnf-list | grep my_first_vnf | awk '{print $2}')
VNFD_ID=$(osm vnf-show $VNF_ID --literal | yq .vnfd-id)

osm ns-update ${NS_ID} \
    --updatetype CHANGE_VNFPKG \
    --config "{changeVnfPackageData: [
        {vnfInstanceId: \"${VNF_ID}\",
        vnfdId: \"$VNFD_ID\"}]}" \
    --timeout 300 \
    --wait

echo "========================================================================"
echo "Done"
echo "========================================================================"

