#!/bin/bash

echo "========================================================================"
echo "Launching network service"
echo "========================================================================"
osm ns-create --ns_name my_first_ns \
    --nsd_name my_first_ns \
    --vim_account openstack \
    --ssh_keys ~/.ssh/id_rsa.pub \
    --config \
    '{vld: [ {name: mgmtnet, vim-network-name: management} ] }'
echo "========================================================================"
echo "Done"
echo "========================================================================"

