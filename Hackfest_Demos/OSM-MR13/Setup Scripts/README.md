## Setting up Participant VMs

First, check the common-vars for the following:

```
APT_PROXY=http://172.21.1.1:3142
FLAVOR=m1.xlarge
PROJECT=f15ab4f845dc4052811fce96a3676ac8
SUBNET=172.21.248
```

APT_PROXY: the address of a caching web proxy for installing packages
FLAVOR: the flavour of the new VM to launch.  Must have at least 4 CPUs, 8 GB RAM and 60 GB Disk.
PROJECT: The project ID in which to launch the new VM
SUBNET: the first three octets of the subnet to use.  IP addresses will be allocated from this subnet, starting with .101 - .199

### Creating Openstack Tenants
This script will create users and projects in Openstack for hackfest-1, -2, -3, etc.  These projects are for deploying workloads to, OSM itself will be installed under the admin tenant.  It also creates SGi, S1, Private and Management networks, and launches OSM, VyOS and Microk8s VMs in the tenant.

```
./run-create-openstack-user-and-project.sh
```

### Initial Configuration
This script updates apt, upgrades the system to the latest 20.04 and installs remote desktop software

```
./run-vm-setup.sh
```

### Install Microk8s

This script will configure the Microk8s VM launched by the create-openstack-user-and-project script.
```
./run-microk8s-setup.sh
```


### Install OSM

This script downloads the installer and runs it.  It also sets the OSM admin password to `hackfest`.
```
./run-install-osm.sh
```

### Add VIM and K8s Cluster

```
./run-osm-vim-k8scluster-add.sh
```

## Full Flow

```
./run-create-openstack-user-and-project.sh 1 1
./run-vm-setup.sh 1 1
./run-microk8s-setup.sh 1 1
./run-install-osm.sh 1 1
./run-osm-vim-k8scluster-add.sh 1 1
```

