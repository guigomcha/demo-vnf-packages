#!/bin/bash
echo $0 started at $(date)

sudo snap install microk8s --classic --channel=1.23/stable
sudo snap install jq

sudo mkdir -p /var/snap/microk8s/current/args/certs.d/docker.io
cat << EOF | sudo tee /var/snap/microk8s/current/args/certs.d/docker.io/hosts.toml
server = "http://172.21.18.3:5000"

[host."http://172.21.18.3:5000"]
capabilities = ["pull", "resolve"]
skip_verify = true
plain-http = true
EOF

sudo systemctl restart snap.microk8s.daemon-containerd.service
sudo microk8s.enable storage
sudo microk8s enable dns:172.21.18.2

sudo snap alias microk8s.kubectl kubectl

wget https://osm-download.etsi.org/ftp/osm-12.0-twelve/install_osm.sh
chmod +x ./install_osm.sh
./install_osm.sh -R testing-daily -r testing  -y --charmed --tag testing-daily 2>&1 | tee install_osm.log

# Set our environment to talk to the new OSM
$(grep "export OSM_PASSWORD" install_osm.log|head -1)
$(grep "export OSM_HOSTNAME" install_osm.log|head -1)

while [ 1 ] ; do
    osm version
    if [ $? -eq 0 ]; then break ; fi
done

# Change the password
osm user-update admin --password hackfest
juju config -m osm mon grafana-password=hackfest

POD=`sudo microk8s.kubectl -n osm get po | grep -v operator | grep grafana- | awk '{print $1}'`
sudo microk8s.kubectl exec -n osm -it $POD -- grafana-cli admin reset-admin-password hackfest

# Expose our services
sudo sed -i "s/127.0.0.1 /127.0.0.1 nbi.osm ui.osm grafana.osm prometheus.osm /" /etc/hosts

juju config -m osm grafana site_url=https://grafana.osm
juju config -m osm prometheus site_url=https://prometheus.osm
juju config -m osm nbi external-hostname=nbi.osm
juju config -m osm ng-ui external-hostname=ui.osm

echo "export OSM_HOSTNAME=nbi.osm:443" >> .bashrc
echo "export OSM_HOSTNAME=nbi.osm:443" >> .profile
echo "export OSM_PASSWORD=hackfest" >> .bashrc
echo "export OSM_PASSWORD=hackfest" >> .profile

# Set some sane defaults for LXD
lxc profile set default limits.memory 1GB
lxc profile set default limits.cpu 2

echo $0 $@ complete at $(date)