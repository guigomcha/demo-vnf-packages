#!/bin/bash
echo $0 started at $(date)

sudo sed -i "s/127.0.0.1 /127.0.0.1 $HOSTNAME /" /etc/hosts
echo 'ubuntu:hackfest'| sudo chpasswd
sudo sed -i "s/^PasswordAuthentication.*/PasswordAuthentication yes/" /etc/ssh/sshd_config
sudo service ssh restart
echo "Acquire::http::Proxy \"http://172.21.18.3:3142\";" | sudo tee /etc/apt/apt.conf.d/proxy.conf
echo "Acquire::https::Proxy \"http://172.21.18.3:3142\";" | sudo tee -a /etc/apt/apt.conf.d/proxy.conf

sudo apt update
sudo apt full-upgrade -y

sudo snap install microk8s --classic --channel=1.23/stable
sudo mkdir -p /var/snap/microk8s/current/args/certs.d/docker.io
cat << EOF | sudo tee /var/snap/microk8s/current/args/certs.d/docker.io/hosts.toml
server = "http://172.21.18.3:5000"

[host."http://172.21.18.3:5000"]
capabilities = ["pull", "resolve"]
skip_verify = true
plain-http = true
EOF

sudo systemctl restart snap.microk8s.daemon-containerd.service
sudo microk8s.enable storage

HOST_IP=10.0.0.11
METALLB_START=10.0.0.201
METALLB_END=10.0.0.250
sudo cat /var/snap/microk8s/current/args/kube-apiserver | grep advertise-address || ( echo "--advertise-address ${HOST_IP}" | sudo tee -a /var/snap/microk8s/current/args/kube-apiserver; sudo microk8s.stop; sudo microk8s.start; )

sudo microk8s.enable metallb:${METALLB_START}-${METALLB_END}
sudo microk8s.enable ingress storage dns
sudo microk8s status --wait-ready
KUBECONFIG=~/kubeconfig.yaml
sudo microk8s config | tee ${KUBECONFIG}

sudo usermod -a -G microk8s ubuntu
sudo chown -f -R ubuntu ~/.kube
sudo snap alias microk8s.kubectl kubectl

echo $0 $@ complete at $(date)