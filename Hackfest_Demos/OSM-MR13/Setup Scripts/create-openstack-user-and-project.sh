#!/bin/bash
echo $0 started at $(date)

. ./common-vars
. ./admin-credentials.rc

PARTICIPANT=${1}

OPENSTACK_USER=hackfest-${PARTICIPANT}
PROJECT=hackfest-${PARTICIPANT}
PASSWORD=hackfest

echo "Creating OpenStack project: ${PROJECT}"
openstack project create --domain ${ADMIN_DOMAIN} ${PROJECT}
PROJECT_ID=`openstack project list | grep "${PROJECT} " | awk '{print $2}'`
openstack quota set --cores 64 --ram 131072 --instances 12 ${PROJECT_ID} &

echo "Creating OpenStack User"
openstack user create --project ${PROJECT} --domain ${ADMIN_DOMAIN} --password $PASSWORD $OPENSTACK_USER
openstack role add --user $OPENSTACK_USER --project ${PROJECT} member

echo "Creating OpenStack Network RBAC policy access_as_external, for network $NETWORK and project ${PROJECT}"
openstack network rbac create \
    --target-project ${PROJECT} \
    --type network \
    --action access_as_external \
    $NETWORK &

echo "Creating OpenStack Network RBAC policy access_as_shared, for network $NETWORK and project ${PROJECT}"
openstack network rbac create \
    --target-project ${PROJECT} \
    --type network \
    --action access_as_shared \
    $NETWORK &

wait

echo "Creating public port"

IP=`expr ${PARTICIPANT} + 0`
VYOSIP=`expr ${PARTICIPANT} + 100`
# This port gets created as the admin
openstack port create --disable-port-security --fixed-ip ip-address=${SUBNET}.${IP} --project=${PROJECT} --enable --network osm-ext hackfest-osm-${PARTICIPANT}
openstack port create --disable-port-security --fixed-ip ip-address=${SUBNET}.${VYOSIP} --project=${PROJECT} --enable --network osm-ext VyOS-management-${PARTICIPANT} &
wait

# This is done as the tenant, and some form of bug makes it necessary to unset this variable
unset OS_PROJECT_NAME
export OS_USERNAME=${OPENSTACK_USER}
export OS_PASSWORD=${PASSWORD}
export OS_PROJECT_NAME=${PROJECT}

NOT_READY=1
while [ $NOT_READY -eq 1 ] ; do
    openstack server list
    NOT_READY=$?
done

echo "Adding security groups"
for i in $(openstack security group list | awk '/default/{ print $2 }'); do
    openstack security group rule create $i --protocol icmp --remote-ip 0.0.0.0/0 &
    openstack security group rule create $i --protocol udp --remote-ip 0.0.0.0/0 &
    openstack security group rule create $i --protocol tcp --remote-ip 0.0.0.0/0 &
done
wait

echo "Creating networks"
NETWORK=management
echo "Creating ${NETWORK} network"
openstack network create ${NETWORK} --enable --no-share
openstack subnet create ${NETWORK}-subnet --network=${NETWORK} --subnet-range=${MANAGEMENT_SUBNET}.0/24 --gateway none --dns-nameserver 172.21.18.2 --dhcp --allocation-pool start=10.0.0.101,end=10.0.0.199 --host-route destination=0.0.0.0/0,gateway=10.0.0.10

NETWORK=SGi
echo "Creating ${NETWORK} network"
openstack network create ${NETWORK} --enable --no-share
openstack subnet create ${NETWORK}-subnet --network=${NETWORK} --subnet-range=${SGi_SUBNET}.0/24 --gateway none --host-route destination=0.0.0.0/0,gateway=10.0.0.10

NETWORK=S1
echo "Creating ${NETWORK} network"
openstack network create ${NETWORK} --enable --no-share
openstack subnet create ${NETWORK}-subnet --network=${NETWORK} --subnet-range=${S1_SUBNET}.0/24

echo "Creating Private network"
openstack network create private --enable --no-share
openstack subnet create private-subnet --network=private --subnet-range=${PRIVATE_SUBNET}.0/24  --gateway none --host-route destination=0.0.0.0/0,gateway=192.168.239.250

# Create as the project user
echo "Creating Ports"
openstack port create --disable-port-security --fixed-ip ip-address=192.168.239.250 --enable --disable-port-security --network private VyOS-private &
openstack port create --disable-port-security --fixed-ip ip-address=10.0.0.10 --enable --network management OSM-management &
openstack port create --disable-port-security --fixed-ip ip-address=10.0.0.11 --enable --network management MK8s-management &


echo "Creating Keypair"
openstack keypair create --public-key ./hackfest_rsa.pub ${KEY_NAME} &
wait

echo "Launching OSM VM"
openstack server create --availability-zone ${ZONE} --key-name ${KEY_NAME} --flavor ${FLAVOR} --image ${FOCAL} --nic port-id=hackfest-osm-${PARTICIPANT} --nic port-id=OSM-management --user-data ./osm-cloud-init.yaml osm-${PARTICIPANT} &
echo "Launching Microk8s VM"
openstack server create --availability-zone ${ZONE} --key-name ${KEY_NAME} --flavor ${FLAVOR} --image ${FOCAL} --nic port-id=MK8s-management -- microk8s &
echo "Launching VyOS Router"
openstack server create --availability-zone ${ZONE} --flavor m1.small --image ${VyOS} --nic port-id=VyOS-management-${PARTICIPANT} --nic port-id=VyOS-private --config-drive True --user-data "`pwd`/vyos-userdata.vsh" vyos-pnf-router
wait

echo "Waiting for OSM VM to be ready"

while [ 1 ] ; do
    sleep 5
    ALIVE=$(ssh -T -o ConnectTimeout=1 -o StrictHostKeyChecking=no -i hackfest_rsa ubuntu@${SUBNET}.${IP} 'cloud-init status --wait | tail -1' 2> /dev/null)
    if [ "${ALIVE}" == "status: done" ] ; then break ; fi
done

echo $0 $@ complete at $(date)