#!/bin/bash
echo $0 started at $(date)

. ./common-vars $@

for PARTICIPANT in `seq ${START} ${MAX}` ; do
  IP=`expr ${PARTICIPANT} + 0`
  scp -o StrictHostKeyChecking=no -i hackfest_rsa ./hackfest_rsa ./hackfest_rsa.pub ubuntu@${SUBNET}.${IP}:.ssh/ &
  scp -o StrictHostKeyChecking=no -i hackfest_rsa vm-microk8s-setup.sh ubuntu@${SUBNET}.${IP}: &
done
wait

for PARTICIPANT in `seq ${START} ${MAX}` ; do
  IP=`expr ${PARTICIPANT} + 0`
  ssh -o StrictHostKeyChecking=no -i hackfest_rsa ubuntu@${SUBNET}.${IP} "scp -o StrictHostKeyChecking=no -i .ssh/hackfest_rsa vm-microk8s-setup.sh 10.0.0.11:; ssh -o StrictHostKeyChecking=no -i .ssh/hackfest_rsa 10.0.0.11 ./vm-microk8s-setup.sh"  2>&1 | tee -a logs/vm-microk8s-setup-${PARTICIPANT}.log&
done
wait

echo $0 $@ complete at $(date)