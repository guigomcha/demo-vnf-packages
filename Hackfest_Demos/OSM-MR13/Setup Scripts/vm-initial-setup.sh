#!/bin/bash
echo $0 started at $(date)

sudo sed -i "s/127.0.0.1 /127.0.0.1 $HOSTNAME /" /etc/hosts
echo 'ubuntu:hackfest'| sudo chpasswd
sudo sed -i "s/^PasswordAuthentication.*/PasswordAuthentication yes/" /etc/ssh/sshd_config
sudo service ssh restart
echo "Acquire::http::Proxy \"http://172.21.18.3:3142\";" | sudo tee /etc/apt/apt.conf.d/proxy.conf
echo "Acquire::https::Proxy \"http://172.21.18.3:3142\";" | sudo tee -a /etc/apt/apt.conf.d/proxy.conf

sudo apt update
sudo apt full-upgrade -y

sudo apt install -y firefox gnome-session xrdp
sudo snap install code --classic
sudo snap install openstackclients yq jq

# Update so buttons show up
gsettings set org.gnome.desktop.wm.preferences button-layout :minimize,maximize,close
gsettings set org.gnome.desktop.interface enable-animations false
# Prevent VM from going to sleep
sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target

cat << EOF | sudo tee /etc/polkit-1/localauthority/50-local.d/color.pkla
[Allow colord for all users]
Identity=unix-user:*
Action=org.freedesktop.color-manager.create-device;org.freedesktop.color-manager.create-profile;org.freedesktop.color-manager.delete-device;org.freedesktop.color-manager.delete-profile;org.freedesktop.color-manager.modify-device;org.freedesktop.color-manager.modify-profile
ResultAny=yes
ResultInactive=yes
ResultActive=yes
EOF

echo net.ipv4.ip_forward=1 | sudo tee -a /etc/sysctl.conf
sudo sysctl net.ipv4.ip_forward=1
sudo iptables -t nat -A POSTROUTING -o ens3 -j MASQUERADE
cat << EOF |  sudo tee /etc/rc.local
#!/bin/sh -e
echo iptables -t nat -A POSTROUTING -o ens3 -j MASQUERADE
EOF
sudo chmod +x /etc/rc.local

git clone --recurse-submodules -j8 https://osm.etsi.org/gitlab/vnf-onboarding/osm-packages.git

echo $0 $@ complete at $(date)
