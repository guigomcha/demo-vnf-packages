#!/bin/bash
echo $0 started at $(date)

. ./common-vars
. ./admin-credentials.rc
mkdir -p logs/

for PARTICIPANT in `seq ${START} ${MAX}` ; do
    ./delete-openstack-user-and-project.sh ${PARTICIPANT} 2>&1 | tee -a logs/delete-openstack-user-and-project-${PARTICIPANT}.log &
done
wait

echo $0 $@ complete at $(date)