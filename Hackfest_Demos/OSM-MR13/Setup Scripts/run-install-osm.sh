#!/bin/bash
echo $0 started at $(date)

. ./common-vars $@

for PARTICIPANT in `seq ${START} ${MAX}` ; do
  IP=`expr ${PARTICIPANT} + 0`
  scp -o StrictHostKeyChecking=no -i hackfest_rsa ./vm-install-osm.sh ubuntu@${SUBNET}.${IP}: &
done
wait

for PARTICIPANT in `seq ${START} ${MAX}` ; do
  IP=`expr ${PARTICIPANT} + 0`
  ssh -o StrictHostKeyChecking=no -i hackfest_rsa ubuntu@${SUBNET}.${IP} ./vm-install-osm.sh 2>&1 | tee -a logs/vm-install-osm-${PARTICIPANT}.log&
done
wait

echo $0 $@ complete at $(date)