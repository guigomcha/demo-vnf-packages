#!/bin/vbash
source /opt/vyatta/etc/functions/script-template
# Interface Config eth0
set interfaces ethernet eth0 address dhcp
set interfaces ethernet eth0 description VyOS-public
# Interface Config eth1
set interfaces ethernet eth1 address dhcp
set interfaces ethernet eth1 description VyOS-private
# System config
set system gateway-address 10.0.0.1
set system host-name vyos-firewall
set service ssh listen-address 0.0.0.0
set service ssh port 22
set system login user osm authentication plaintext-password osm2021
# Enable masquerading
set nat source rule 100 outbound-interface 'eth0'
set nat source rule 100 source address '192.168.239.0/24'
set nat source rule 100 translation address masquerade
# SNMP
set service snmp community public authorization ro
set service snmp location "OSM Labs"
set service snmp contact "osmsupport@etsi.org"
# Save
commit
save