#!/bin/bash
echo $0 started at $(date)

. ./common-vars $@
. ./admin-credentials.rc

for PARTICIPANT in `seq ${START} ${MAX}` ; do

    IP=`expr ${PARTICIPANT} + 0`

    OS_USERNAME=hackfest-${PARTICIPANT}
    OS_PASSWORD=hackfest
    OS_PROJECT_NAME=hackfest-${PARTICIPANT}

    cat << EOF > ${OS_USERNAME}.rc
export OS_AUTH_TYPE=password
export OS_AUTH_URL=${OS_AUTH_URL}
export OS_CACERT=/home/ubuntu/pc-cacert.pem
export OS_DOMAIN_NAME=${OS_DOMAIN_NAME}
export OS_IDENTITY_API_VERSION=3
export OS_INTERFACE=public
export OS_PASSWORD=${OS_PASSWORD}
export OS_PROJECT_DOMAIN_NAME=${OS_DOMAIN_NAME}
export OS_PROJECT_NAME=${OS_PROJECT_NAME}
export OS_REGION_NAME=${OS_REGION_NAME}
export OS_TENANT_NAME=${OS_USERNAME}
export OS_USERNAME=${OS_USERNAME}
export OS_USER_DOMAIN_NAME=${OS_DOMAIN_NAME}
EOF

    ssh -o StrictHostKeyChecking=no -i hackfest_rsa ubuntu@${SUBNET}.${IP} ". .profile;osm version"
    while [ $? -ne -0 ] ; do
        ssh -o StrictHostKeyChecking=no -i hackfest_rsa ubuntu@${SUBNET}.${IP} ". .profile;osm version"
    done
    scp -o StrictHostKeyChecking=no -i hackfest_rsa ${OS_USERNAME}.rc ubuntu@${SUBNET}.${IP}:
    scp -o StrictHostKeyChecking=no -i hackfest_rsa pc-cacert.pem ubuntu@${SUBNET}.${IP}:

    ssh -o StrictHostKeyChecking=no -i hackfest_rsa ubuntu@${SUBNET}.${IP} ". .profile;osm vim-create --name openstack --user ${OS_USERNAME} --password ${OS_PASSWORD} --auth_url ${OS_AUTH_URL} --tenant ${OS_PROJECT_NAME} --account_type openstack --config=\"{ management_network_name: osm-ext, security_groups: default, insecure: true, project_domain_name: ${OS_PROJECT_DOMAIN_NAME}, user_domain_name: ${OS_USER_DOMAIN_NAME} }\"" 2>&1 | tee -a logs/osm-vim-k8scluster-add-${PARTICIPANT}.log
    ssh -o StrictHostKeyChecking=no -i hackfest_rsa ubuntu@${SUBNET}.${IP} "scp -o StrictHostKeyChecking=no -i .ssh/hackfest_rsa ${MANAGEMENT_SUBNET}.11:kubeconfig.yaml ." 2>&1 | tee -a logs/osm-vim-k8scluster-add-${PARTICIPANT}.log
    ssh -o StrictHostKeyChecking=no -i hackfest_rsa ubuntu@${SUBNET}.${IP} '. .profile;osm k8scluster-add --creds kubeconfig.yaml --vim openstack --k8s-nets "{"net1": "osm-ext"}" --version 1.23 --namespace hackfest --description "Microk8s cluster" hackfest' 2>&1 | tee -a logs/osm-vim-k8scluster-add-${PARTICIPANT}.log
    ssh -o StrictHostKeyChecking=no -i hackfest_rsa ubuntu@${SUBNET}.${IP} "echo . ~/${OS_USERNAME}.rc >> .bashrc"
done

echo $0 $@ complete at $(date)