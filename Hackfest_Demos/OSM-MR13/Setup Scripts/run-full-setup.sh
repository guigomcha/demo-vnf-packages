#!/bin/bash
echo $0 started at $(date)

. common-vars

./run-create-openstack-user-and-project.sh $@
./run-vm-initial-setup.sh $@
./run-microk8s-setup.sh $@ &
./run-install-osm.sh $@
wait
./run-osm-vim-k8scluster-add.sh $@

echo $0 $@ complete at $(date)