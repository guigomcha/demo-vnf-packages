#!/bin/bash
export MAGMA_NS_NAME=magma_orc_ns
echo "========================================================================"
echo "Getting admin operator credentials"
echo "========================================================================"
output=`osm ns-action $MAGMA_NS_NAME --vnf_name magma_orc_cnf --kdu_name magma-orc-kdu --action_name get-pfx-package-password --wait`
echo "========================================================================"
echo "pfx_package pass appears in the operation output, please save it"
echo "========================================================================"
echo $output
