#!/bin/bash
echo "========================================================================"
echo "Open the browser and import the admin_operator.pfx using certificate import."
echo "When it asks you a password, please enter pfx_package pass."
echo "Try to reach following URL using your browser: https://master.nms.osm.magma.com."
echo "Use the admin-username and admin-password to login"
echo "========================================================================"