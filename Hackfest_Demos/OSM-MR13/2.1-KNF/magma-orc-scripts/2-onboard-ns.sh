#!/bin/bash
export MAGMA_NS_NAME=magma_orc_ns
export VIM_ACCOUNT=openstack
export K8S_NET=osm-ext
echo "========================================================================"
echo "Creating MAGMA_NS: $MAGMA_NS_NAME"
echo "========================================================================"
osm ns-create --ns_name $MAGMA_NS_NAME --nsd_name magma_orc_cnf_ns --vim_account $VIM_ACCOUNT --config "{vld: [ {name: mgmtnet, vim-network-name: $K8S_NET}]}"
# Check NS status
osm ns-list

echo "========================================================================"
echo "Check NS status using osm ns-list, osm ns-show $MAGMA_NS_NAME"
echo "========================================================================"
