#!/bin/bash
export MAGMA_NS_NAME=magma_orc_ns
echo "========================================================================"
echo "Getting Magma orchestrator GUI credentials"
echo "========================================================================"
osm ns-action $MAGMA_NS_NAME --vnf_name magma_orc_cnf --kdu_name magma-orc-kdu --action_name get-master-admin-credentials --wait

echo "========================================================================"
echo "admin-password and admin-username appear in the operation output, please save it"
echo "========================================================================"
