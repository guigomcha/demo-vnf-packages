#!/bin/bash

export orc_model=`juju models |  grep -i magma-orc | awk -F " " '{print $1}' | tr -d \*`
juju switch $orc_model
echo $orch_model
echo "========================================================================"
echo "Dowloading admin_operator.pfx file to your current working directory"
echo "========================================================================"
juju scp --container="magma-orc8r-certifier" orc8r-certifier/0:/var/opt/magma/certs/admin_operator.pfx admin_operator.pfx

