#!/bin/bash

export PDNS_NS_NAME=powerdns_ns
export VNF_NAME=powerdns
export KDU_NAME=powerdns

read -p "Enter ZONE:" ZONE

# Check if string is empty using -z. For more 'help test'
if [[ -z "$ZONE" ]]; then
   printf '%s\n' "No input entered"
   exit 1
else
   printf "You entered ZONE %s " "$ZONE"
fi

echo "========================================================================"
echo "Adding zone"
echo "========================================================================"

ADD_ZONE_OP_ID=`osm ns-action --action_name add-zone --vnf_name $VNF_NAME --kdu_name $KDU_NAME --params "{"zone_name": $ZONE}" $PDNS_NS_NAME`

echo "========================================================================"
echo "Showing action status"
echo "========================================================================"

# Check operation status
osm ns-op-show $ADD_ZONE_OP_ID

echo "========================================================================"
echo "Check action status using osm ns-op-show $ADD_ZONE_OP_ID"
echo "========================================================================"

