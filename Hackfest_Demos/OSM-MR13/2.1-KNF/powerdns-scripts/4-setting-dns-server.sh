#!/bin/bash

export PDNS_NS_NAME=powerdns_ns
VNF_ID=`osm vnf-list --ns $PDNS_NS_NAME | grep powerdns | awk '{print $2}'`

echo "========================================================================"
echo "VNF ID $VNF_ID"
echo "========================================================================"

export DNS_IP=`osm vnf-show $VNF_ID --literal | yq -e '.kdur[0].services[] | select(.name | contains("udp")) | .external_ip' | tr -d ' '-`

echo "========================================================================"
echo "DNS IP $DNS_IP"
echo "========================================================================"

echo "========================================================================"
echo "Setting DNS Server in your machine"
echo "========================================================================"
sudo  sed -i "1s/^/nameserver $DNS_IP\n/"  /etc/resolv.conf
