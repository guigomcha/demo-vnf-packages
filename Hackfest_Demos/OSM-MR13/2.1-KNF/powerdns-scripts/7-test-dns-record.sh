#!/bin/bash

export PDNS_NS_NAME=powerdns_ns
export VNF_NAME=powerdns
export KDU_NAME=powerdns


read -p "Enter DNS RECORD:" RECORD
if [[ -z "$RECORD" ]]; then
   printf '%s\n' "No input entered"
   exit 1
else
   printf "You entered RECORD %s " "$RECORD"
fi


echo "========================================================================"
echo "Testing record"
echo "========================================================================"

# Sample record: "test.example.org"

dig $RECORD

echo "========================================================================"
echo "Testing the record using dig @${DNS_IP} $RECORD"
echo "========================================================================"
