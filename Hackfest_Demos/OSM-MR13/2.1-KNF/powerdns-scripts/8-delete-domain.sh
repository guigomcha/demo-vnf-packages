#!/bin/bash

export PDNS_NS_NAME=powerdns_ns
export VNF_NAME=powerdns
export KDU_NAME=powerdns

read -p "Enter ZONE:" ZONE

if [[ -z "$ZONE" ]]; then
   printf '%s\n' "No input entered"
   exit 1
else
   printf "You entered ZONE %s " "$ZONE"
fi


read -p "Enter DOMAIN:" DOMAIN
if [[ -z "$DOMAIN" ]]; then
   printf '%s\n' "No input entered"
   exit 1
else
   printf "You entered DOMAIN %s " "$DOMAIN"
fi


echo "========================================================================"
echo "Deleting Domain"
echo "========================================================================"

# Delete Domain
DEL_DOM_OP_ID=`osm ns-action --action_name delete-domain --vnf_name $VNF_NAME --kdu_name $KDU_NAME  --params "{'zone_name': $ZONE, 'subdomain': $DOMAIN}" $PDNS_NS_NAME`

echo "========================================================================"
echo "Showing action status"
echo "========================================================================"

osm ns-op-show $DEL_DOM_OP_ID

echo "========================================================================"
echo "Check the action status using osm ns-op-show $DEL_DOM_OP_ID"
echo "========================================================================"
