#!/bin/bash
echo "========================================================================"
echo "Downloading OSM packages"
echo "========================================================================"

git clone --recurse-submodules -j8 https://osm.etsi.org/gitlab/vnf-onboarding/osm-packages.git

