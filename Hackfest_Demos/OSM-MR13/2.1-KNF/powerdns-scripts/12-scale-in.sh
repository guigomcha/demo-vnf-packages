#!/bin/bash

export PDNS_NS_NAME=powerdns_ns
export VNF_NAME=powerdns
export KDU_NAME=powerdns
export VNF_ID=`osm vnf-list --ns $PDNS_NS_NAME | grep powerdns | awk '{print $2}'`

echo "========================================================================"
echo "Rolling back"
echo "========================================================================"

SCALE_IN_OP_ID=`osm ns-action --action_name rollback --vnf_name $VNF_NAME --kdu_name $KDU_NAME $PDNS_NS_NAME`

echo "========================================================================"
echo "Check the action status using osm ns-op-show $SCALE_IN_OP_ID"
echo "Showing action status"
echo "========================================================================"

osm ns-op-show $SCALE_IN_OP_ID --literal | yq .operationState

echo "========================================================================"
echo "Check the replicatCount number using osm vnf-show $VNF_ID --kdu $KDU_NAME | yq .config.replicaCount"
echo "Showing replicaCount number"
echo "========================================================================"
osm vnf-show $VNF_ID --kdu $KDU_NAME | yq .config.replicaCount