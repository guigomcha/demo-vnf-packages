#!/bin/bash
echo "========================================================================"
echo "Setting env variables"
echo "========================================================================"
export K8S_NET=osm-ext
export PDNS_NS_NAME=powerdns_ns

echo "========================================================================"
echo "Deploying NS"
echo "========================================================================"

osm ns-create --ns_name $PDNS_NS_NAME --nsd_name powerdns_ns --vim_account openstack --config "{vld: [ {name: mgmtnet, vim-network-name: $K8S_NET}]}"
osm ns-list
echo "========================================================================"
echo "NS_NAME: $PDNS_NS_NAME"
echo "NS_ID: $PDNS_NS_NAME deployed"
echo "========================================================================"

echo "========================================================================"
echo "Check NS status using osm ns-list, osm ns-show $PDNS_NS_NAME"
echo "========================================================================"


