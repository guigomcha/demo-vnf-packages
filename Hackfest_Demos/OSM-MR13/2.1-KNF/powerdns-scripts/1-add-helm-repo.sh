#!/bin/bash

sudo snap install yq
echo "========================================================================"
echo "Adding Helm Repository"
echo "========================================================================"
osm repo-add --type helm-chart --description "Repository for Powerdns helm Chart" osm-helm https://gatici.github.io/helm-repo/
echo "========================================================================"
echo "Listing Helm Repository"
echo "========================================================================"
osm repo-list
