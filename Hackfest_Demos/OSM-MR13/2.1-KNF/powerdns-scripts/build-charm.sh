#!/bin/bash

echo "========================================================================"
echo "Install charmcraft"
echo "========================================================================"
sudo snap install charmcraft --classic
cd  ../../powerdns/powerdns_knf/charms/ops/

if [ ! -d powerdns-operator ] ; then
    echo "It does not look like we are in osm-packages, exiting"
    exit 1
fi
echo "========================================================================"
echo "Packing charm"
echo "========================================================================"
pushd powerdns-operator
charmcraft pack
echo "========================================================================"
echo "Copying charm under VNFD/charms folder"
echo "========================================================================"
cp powerdns-operator_ubuntu-20.04-amd64.charm  ../../charms
popd
