#!/bin/bash

export PDNS_NS_NAME=powerdns_ns
export VNF_NAME=powerdns
export KDU_NAME=powerdns
export VNF_ID=`osm vnf-list --ns $PDNS_NS_NAME | grep powerdns | awk '{print $2}'`

read -p "Enter replicaCount number to scale as number:" NUM

if [[ -z "$NUM" ]]; then
   printf '%s\n' "No input entered"
   exit 1
else
   printf "You entered NUM %s " "$NUM"
fi

echo "========================================================================"
echo "Scaling out"
echo "========================================================================"

SCALE_OUT_OP_ID=`osm ns-action --action_name upgrade --vnf_name $VNF_NAME  --kdu_name $KDU_NAME --params "{'replicaCount':$NUM,}" $PDNS_NS_NAME`

echo "========================================================================"
echo "Check the action status using osm ns-op-show $SCALE_OUT_OP_ID --literal | yq .operationState"
echo "Showing action status"
echo "========================================================================"
osm ns-op-show $SCALE_OUT_OP_ID --literal | yq .operationState

echo "========================================================================"
echo "Check the replicaCount using osm vnf-show $VNF_ID --kdu $KDU_NAME | yq .config.replicaCount"
echo "Showing replicaCount number"
echo "========================================================================"
osm vnf-show $VNF_ID --kdu $KDU_NAME | yq .config.replicaCount
