# Magma Orchestrator NS

Descriptors that installs magma-orc8r using [Charmhub magma-orc8r bundle](https://charmhub.io/magma-orc8r?channel=beta)
Orchestrator is a Magma service that provides a simple and consistent way to configure and monitor the wireless network securely.
The metrics acquired through the platform allows you to see the analytics and traffic flows of the wireless users through the Magma web UI.
For more information about Magma, see the official documentation [here](https://magmacore.org/).

## Download Packages

```bash
git clone --recurse-submodules -j8 https://osm.etsi.org/gitlab/vnf-onboarding/osm-packages.git
```

## Create the VIM Account

```bash
# This is dummy vim account
export VIM_ACCOUNT=openstack
osm vim-create --name $VIM_ACCOUNT \
               --account_type dummy \
               --user dummy \
                --password dummy \
                --auth_url "http://dummy" \
                --tenant dummy
# Check the VIM account availability
osm vim-list
osm vim-show $VIM_ACCOUNT
```

## Add K8s Cluster

```bash
# kubeconfig.yaml exists in the HOME directory
export K8S_NET=osm-ext
export k8S_CLS_NAME=hackfest
osm k8scluster-add --creds ~/kubeconfig.yaml \
                     --vim k8s-vim \
                     --k8s-nets "{k8s_net: $K8S_NET}" \
                     --version 1.24 \
                     $K8S_CLS_NAME
# Check K8s cluster availability
osm k8scluster-list
osm k8scluster-show $K8S_K8S_CLS_NAME          
```


## Onboarding and instantiation

```bash
# Define the NS name
export MAGMA_NS_NAME=<ns name>
```

```bash
osm nfpkg-create magma_orc_cnf
osm nspkg-create magma_orc_ns
osm ns-create --ns_name $MAGMA_NS_NAME --nsd_name magma_orc_cnf_ns --vim_account $VIM_ACCOUNT --config "{vld: [ {name: mgmtnet, vim-network-name: $K8S_NET}]}"
# Check NS status
osm ns-list
```

## Download the Admin Operator Https Certificate

```bash
export orc_model=juju models |  grep -i magma-orc | awk -F " " '{print $1}' | tr -d \*
juju switch $orch_model
# admin_operator.pfx file should appear in the local path
juju scp --container="magma-orc8r-certifier" orc8r-certifier/0:/var/opt/magma/certs/admin_operator.pfx admin_operator.pfx
```

## Run Day2 actions

### Get the Admin Operator Https Certificate credentials

```bash
osm ns-action $MAGMA_NS_NAME --vnf_name magma_orc_cnf --kdu_name magma-orc-kdu --action_name get-pfx-package-password --wait
# pfx_package pass appears in the operation output, please save it
```

### Get the Magma Orchestrator GUI credentials

```bash
osm ns-action $MAGMA_NS_NAME --vnf_name magma_orc_cnf --kdu_name magma-orc-kdu --action_name get-master-admin-credentials --wait
# admin-password and admin-username appear in the operation output, please save it
```

### Get Load Balancer Services

```bash
LOAD_BALANCER_OP_ID=`osm ns-action $NS_NAME --vnf_name magma_orc_cnf --kdu_name magma-orc-kdu --action_name get-load-balancer-services`
osm ns-op-show $LOAD_BALANCER_OP_ID
# Load Balancer service names and External IP's are visible in the action output.
'nginx-proxy':              <nginx-proxy External IP>              ->   master.nms.osm.magma.com
'orc8r-bootstrap-nginx':    <orc8r-bootstrap-nginx External IP>    ->   bootstrapper-controller.osm.magma.com
'orc8r-clientcert-nginx':   <orc8r-clientcert-nginx External IP>   ->   controller.osm.magma.com
'orc8r-nginx-proxy':        <orc8r-nginx-proxy External IP>        ->   api.osm.magma.com
```

### User a DNS server register following Domains

```bash
# Register the domains in PowerDNS server by running day2 actions:
ZONE=osm.magma.com.
# Sample registration for api.osm.magma.com
DOMAIN=api.
IP=<orc8r-nginx-proxy External IP> 
# Test your records availability
# DNS_IP is your DNS server ip which is available as an environment variable.
dig @${DNS_IP} api.osm.magma.com
# Resolve the domain name directly from your machine.
dig api.osm.magma.com
```


## Access to Magma Orchestrator GUI

Open the browser and import the admin_operator.pfx using certificate import.

When it asks you a password, please enter pfx_package pass.

Try to reach following URL using your browser: https://master.nms.osm.magma.com.

Use the admin-username and admin-password to login.


