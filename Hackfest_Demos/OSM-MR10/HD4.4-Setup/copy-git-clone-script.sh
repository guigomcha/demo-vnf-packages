#!/bin/bash

IP=`osm vnf-list | grep virtual-pc | awk '{print $14}'`
echo "========================================================================"
echo Copying git clone script to your remote desktop ${IP}
echo "========================================================================"

sshpass -p osm2021 scp ~/Hackfest/HD4.4-Setup/git-clone.sh ubuntu@${IP}:
sshpass -p osm2021 ssh ubuntu@${IP} chmod +x git-clone.sh

echo "========================================================================"
echo "Done"
echo "========================================================================"

