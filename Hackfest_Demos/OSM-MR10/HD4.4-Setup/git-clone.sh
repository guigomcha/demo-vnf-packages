#!/bin/bash

rm -rf git
mkdir -p git
cd git

for module in IM LCM MON N2VC NBI NG-UI PLA POL RO common osmclient tests ; do
    echo "========================================================================"
    echo "Fetching ${module}"
    git clone "https://osm.etsi.org/gerrit/osm/${module}"
done

echo "========================================================================"
echo "Done"
echo "========================================================================"

