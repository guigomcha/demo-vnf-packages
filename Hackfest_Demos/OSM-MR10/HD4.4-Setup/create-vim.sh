#!/bin/bash

if [ ! -f osm-vim-create.sh ] ; then
	echo "This script must be run from your home directory"
	exit 1
fi
sed -i "s|ca_cert: /canonical.pem|insecure: true|" osm-vim-create.sh
IP=`osm vnf-list | grep virtual-pc | awk '{print $14}'`
echo "========================================================================"
echo Copying vim create script to your remote desktop ${IP}
echo "========================================================================"

sshpass -p osm2021 scp osm-vim-create.sh ubuntu@${IP}:
sshpass -p osm2021 ssh ubuntu@${IP} chmod +x osm-vim-create.sh

echo "========================================================================"
echo "Done"
echo "========================================================================"

