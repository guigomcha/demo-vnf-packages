#!/bin/bash

echo "========================================================================"
echo "Here are some of the actions you can run"
echo "========================================================================"

cat << 'EOF'

DESKTOP_IP=`osm ns-show virtual-desktop --literal | yq e '.vcaStatus.*.machines.0.network_interfaces.ens3.ip_addresses.0' -`

osm ns-action firewall --vnf_name VYOS-PNF --action_name add-port-forward --params "{ruleNumber: '10', sourcePort: '3389', destinationAddress: \"${DESKTOP_IP}\", destinationPort: '3389'}"
osm ns-action firewall --vnf_name VYOS-PNF --action_name remove-port-forward --params '{ruleNumber: "10"}'

EOF 
