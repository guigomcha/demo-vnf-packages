#!/bin/bash

if [ ! -d hackfest_firewall_pnf ] ; then
    echo "It does not look like we are in the osm-packages directory, exiting"
    exit 1
fi

echo "========================================================================"
echo "Building operator charms"
echo "========================================================================"
cd hackfest_firewall_pnf/charms/vyos-config-src
#virtualenv -p python3 venv
#source venv/bin/activate
#pip install -r requirements-dev.txt
#pip install charmcraft
#./venv/bin/charmcraft build
rm -rf venv
charmcraft build
cd -
cd hackfest_firewall_pnf/charms
mkdir -p vyos-config/
rm -rf vyos-config/*
cp -r vyos-config-src/build/* vyos-config/
cd -

echo "========================================================================"
echo "Cleaning out any prior versions of the descriptors from OSM"
echo "========================================================================"
osm nsd-delete hackfest_firewall_pnf_ns
osm vnfd-delete hackfest_firewall_pnf
osm pdu-delete router01
rm -v hackfest_firewall_pnf*.tar.gz

echo "========================================================================"
echo "Building packages"
echo "========================================================================"
osm package-build hackfest_firewall_pnf
osm package-build hackfest_firewall_pnf_ns

echo "========================================================================"
echo "Uploading packages"
echo "========================================================================"
osm upload-package hackfest_firewall_pnf.tar.gz
osm upload-package hackfest_firewall_pnf_ns.tar.gz

VIMID=`osm vim-list | grep osm_ | awk '{ print $4 }'`
echo "========================================================================"
echo "Registering PDU 172.21.19.${HFID} with $VIMID"
echo "========================================================================"

cat << EOF > firewall-pdu.yaml
name: router01
description: VyOS Router
type: gateway
shared: false
interfaces:
 -  name: gateway_public
    ip-address: 172.21.19.${HFID}
    mgmt: true
    vim-network-name: osm-ext
 -  name: vnf_internal
    ip-address: 192.168.239.250
    mgmt: false
    vim-network-name: private
EOF

osm pdu-create --descriptor_file firewall-pdu.yaml \
               --vim_account $VIMID
echo "========================================================================"
echo "Done"
echo "========================================================================"

