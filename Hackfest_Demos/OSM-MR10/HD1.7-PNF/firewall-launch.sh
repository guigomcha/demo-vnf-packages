#!/bin/bash

VIMID=`osm vim-list | grep osm_ | awk '{ print $4 }'`
echo "========================================================================"
echo "Launching network service with VIMID ${VIMID}"
echo "========================================================================"

osm ns-create --ns_name firewall \
    --nsd_name hackfest_firewall_pnf_ns \
    --vim_account $VIMID

echo "========================================================================"
echo "Done"
echo "========================================================================"

