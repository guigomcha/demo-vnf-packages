#!/bin/bash

VIMID=`osm vim-list | grep osm_ | awk '{ print $4 }'`


USER_ID=$OSM_USER
NSD_NAME=wiki_webserver_autoscale_ns_$USER_ID


echo "========================================================================"
echo "Launching network service with VIMID ${VIMID}"
echo "========================================================================"
osm ns-create --ns_name wiki \
    --nsd_name wiki_webserver_autoscale_ns_$USER_ID \
    --vim_account ${VIMID}
echo "========================================================================"
echo "Done"
echo "========================================================================"

