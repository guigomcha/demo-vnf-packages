#!/bin/bash

VIMID=`osm vim-list | grep $OSM_PROJECT | awk '{ print $4 }'`
echo "========================================================================"
echo "Launching network service in VIM with ID ${VIMID}"
echo "========================================================================"
osm ns-create --ns_name ldap \
    --nsd_name openldap_ns \
    --vim_account ${VIMID} \
    --config_file $HOME/openldap-params.yaml
echo "========================================================================"
echo "Done"
echo "========================================================================"

