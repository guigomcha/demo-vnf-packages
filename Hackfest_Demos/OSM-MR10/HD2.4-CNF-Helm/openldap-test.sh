#!/bin/bash

echo "========================================================================"
echo "Getting IP address of the LDAP server"
echo "========================================================================"
VNF_ID=`osm vnf-list --ns ldap|grep openldap |awk '{print $2}'`
IP_ADDR1=`osm vnf-show ${VNF_ID} --literal | yq e '.kdur[0].services[0].external_ip[0]' -`
PROJECT_ID=`osm project-list | grep $OSM_PROJECT | awk '{ print $4 }'`
IP_ADDR2=`kubectl -n ${PROJECT_ID} get svc|grep stable-openldap |awk '{print $4}'`
LB_IP=${IP_ADDR1}
[ "${LB_IP}" == "null" ] && LB_IP=""
[ -n "${LB_IP}" ] || LB_IP=${IP_ADDR2}
echo $LB_IP
echo "========================================================================"
echo "Testing LDAP server"
echo "========================================================================"
ldapsearch -x -H ldap://${LB_IP}:389 -b dc=example,dc=org -D "cn=admin,dc=example,dc=org" -w osm4u
echo "========================================================================"
echo "Done"
echo "========================================================================"

