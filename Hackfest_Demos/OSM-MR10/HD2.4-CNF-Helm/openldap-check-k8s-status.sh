#!/bin/bash

PROJECT_ID=`osm project-list | grep $OSM_PROJECT | awk '{ print $4 }'`
kubectl -n ${PROJECT_ID} get all
echo "========================================================================"
echo "Done"
echo "========================================================================"

