#!/bin/bash

if [ ! -d openldap_knf ] ; then
    echo "It does not look like we are in the osm-packages directory, exiting"
    exit 1
fi

echo "========================================================================"
echo "Cleaning out any prior versions of the descriptors from OSM"
echo "========================================================================"
osm nspkg-delete openldap_ns
osm nfpkg-delete openldap_knf

echo "========================================================================"
echo "Validating packages"
echo "========================================================================"
osm package-validate --no-recursive openldap_knf
osm package-validate --no-recursive openldap_ns

echo "========================================================================"
echo "Building packages"
echo "========================================================================"
osm package-build openldap_knf
osm package-build openldap_ns

echo "========================================================================"
echo "Uploading packages"
echo "========================================================================"
osm nfpkg-create openldap_knf.tar.gz
osm nspkg-create openldap_ns.tar.gz
echo "========================================================================"
echo "Done"
echo "========================================================================"

