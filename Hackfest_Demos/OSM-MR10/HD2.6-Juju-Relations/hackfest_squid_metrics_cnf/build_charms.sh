#!/bin/bash

CHARM_NAMES="grafana-operator prometheus-operator squid-operator"

cd charms/ops
for charm in $CHARM_NAMES; do
    echo "Building charm $charm"
    cd $charm
    charmcraft build
    mkdir -p ../../$charm
    rm -rf ../../$charm/*
    mv build/* ../../$charm/
    cd ..
done
cd ../..
