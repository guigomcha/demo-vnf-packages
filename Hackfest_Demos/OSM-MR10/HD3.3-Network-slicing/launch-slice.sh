#!/bin/bash

if [ ! -f hackfest_squid_metrics_cnf_ns.tar.gz ] ; then
    echo "It does not look like we are in the HD3.3-Network-slicing directory, exiting"
    exit 1
fi

echo "========================================================================"
echo "Cleaning out any prior versions of the descriptors from OSM"
echo "========================================================================"

osm nsd-delete openldap_ns
osm nsd-delete wiki_webserver_autoscale_ns
osm nsd-delete squid_metrics_cnf_ns
osm nsd-delete hackfest_firewall_pnf_ns
osm nsd-delete hackfest_virtual-pc_ns

osm vnfd-delete openldap_knf
osm vnfd-delete wiki_webserver_autoscale_vnf
osm vnfd-delete squid_metrics_cnf
osm vnfd-delete hackfest_firewall_pnf
osm vnfd-delete hackfest_virtual-pc_vnf

echo "========================================================================"
echo "Uploading packages"
echo "========================================================================"

osm upload-package hackfest_firewall_pnf.tar.gz
osm upload-package hackfest_squid_metrics_cnf.tar.gz
osm upload-package hackfest_virtual-pc_vnfd.tar.gz
osm upload-package openldap_knf.tar.gz
osm upload-package wiki_webserver_autoscale_vnfd.tar.gz

osm upload-package openldap_ns.tar.gz
osm upload-package wiki_webserver_autoscale_nsd.tar.gz
osm upload-package hackfest_squid_metrics_cnf_ns.tar.gz
osm upload-package hackfest_firewall_pnf_ns.tar.gz
osm upload-package hackfest_virtual-pc_ns.tar.gz

osm nst-create slice_hackfest_nst.yaml


echo "========================================================================"
echo "Launching first network slice"
echo "========================================================================"

sed -i "s/loadBalancerIP:.*/loadBalancerIP: '172.21.251.${HFID}'/" params.yaml
osm nsi-create --nsi_name virtual-desktop-env1 --nst_name slice_hackfest_nst --vim_account $OSM_USER --config_file params.yaml

