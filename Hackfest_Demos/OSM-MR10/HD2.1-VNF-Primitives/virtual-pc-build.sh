#!/bin/bash

if [ ! -d hackfest_virtual-pc_vnfd ] ; then
    echo "It does not look like we are in the osm-packages directory, exiting"
    exit 1
fi

cd hackfest_virtual-pc_vnfd/charms/virtual-pc-src

echo "========================================================================"
echo "Building operator charms"
echo "========================================================================"
rm -rf venv
charmcraft build
cd -
cd hackfest_virtual-pc_vnfd/charms
cp -r virtual-pc-src/build/* virtual-pc/
cd -

echo "========================================================================"
echo "Cleaning out any prior versions of the descriptors from OSM"
echo "========================================================================"
osm nsd-delete hackfest_virtual-pc_ns
osm vnfd-delete hackfest_virtual-pc_vnf

echo "========================================================================"
echo "Building packages"
echo "========================================================================"
osm package-build hackfest_virtual-pc_vnfd
osm package-build hackfest_virtual-pc_ns

echo "========================================================================"
echo "Uploading packages"
echo "========================================================================"
osm upload-package hackfest_virtual-pc_vnfd.tar.gz
osm upload-package hackfest_virtual-pc_ns.tar.gz
echo "========================================================================"
echo "Done"
echo "========================================================================"

