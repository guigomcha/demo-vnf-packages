#!/bin/bash
echo "========================================================================"
echo "Here are some of the actions you can run"
echo "========================================================================"

cat << 'EOF'
osm ns-action virtual-desktop --vnf_name 1 --action_name update-system
osm ns-action virtual-desktop --vnf_name 1 --action_name add-package --params '{package: "ubuntu-mate-wallpapers-disco,ubuntu-mate-wallpapers-eoan"}'
osm ns-action virtual-desktop --vnf_name 1 --action_name remove-package --params '{package: "ubuntu-mate-wallpapers-disco"}'
osm ns-action virtual-desktop --vnf_name 1 --action_name add-snap --params '{package: "code --classic"}'
osm ns-action virtual-desktop --vnf_name 1 --action_name remove-snap --params '{package: "code"}'
osm ns-action virtual-desktop --vnf_name 1 --action_name reboot
osm ns-action virtual-desktop --vnf_name 1 --action_name announce --params '{message: "Hello from the Hackfest!"}'
EOF

