#!/bin/bash

VIMID=`osm vim-list | grep osm_ | awk '{ print $4 }'`
echo "========================================================================"
echo "Launching network service with VIMID ${VIMID}"
echo "========================================================================"
osm ns-create --ns_name virtual-desktop \
    --nsd_name hackfest_virtual-pc_ns \
    --vim_account ${VIMID} \
    --config \
    '{vld: [ {name: mgmtnet, vim-network-name: osm-ext},
             {name: private, vim-network-name: private} ] }'
echo "========================================================================"
echo "Done"
echo "========================================================================"

