#!/bin/bash

VIMID=`osm vim-list | grep osm_ | awk '{ print $4 }'`
echo "========================================================================"
echo "Launching network service with VIMID ${VIMID}"
echo "========================================================================"
osm ns-create --ns_name basic-vnf \
    --nsd_name hackfest_basic-ns \
    --vim_account ${VIMID} \
    --ssh_keys ~/.ssh/id_rsa.pub \
    --config \
    '{vld: [ {name: mgmtnet, vim-network-name: osm-ext} ] }'
echo "========================================================================"
echo "Done"
echo "========================================================================"

