#!/bin/bash

if [ ! -d hackfest_basic_ns ] ; then
    echo "It does not look like we are in the osm-packages directory, exiting"
    exit 1
fi


echo "========================================================================"
echo "Cleaning out any prior versions of the descriptors from OSM"
echo "========================================================================"
osm nsd-delete hackfest_basic-ns
osm vnfd-delete hackfest_basic-vnf

echo "========================================================================"
echo "Building packages"
echo "========================================================================"
osm package-build hackfest_basic_vnf
osm package-build hackfest_basic_ns

echo "========================================================================"
echo "Uploading packages"
echo "========================================================================"
osm upload-package hackfest_basic_vnf.tar.gz
osm upload-package hackfest_basic_ns.tar.gz
echo "========================================================================"
echo "Done"
echo "========================================================================"

