# Introduction

The NS (k8s\_juju) consists of 1 deployer (k8s\_jujucontroller\_vnf) and 4 nodes (k8s\_jujumachine\_vnf)
connected to a single network or vld (mgmtnet).

The deployer is a Kubernetes installer based on Juju: it will configure the other 4 nodes to run a Kubernetes cluster. Behind the scenes, the deployer is a Juju controller where the 4 nodes are manually added to a Juju model, then a juju bundle is deployed on that model.

# Onboarding and instantiation

```bash
osm nfpkg-create k8s_jujumachine_vnf.tar.gz
osm nfpkg-create k8s_jujucontroller_vnf.tar.gz
osm nspkg-create k8s_juju_ns.tar.gz
```

# Instantiation

Instantiation parameters are controlled by `config.yaml`. The relevant parameters are the network in the VIM where the K8s API will eb exposed and where all nodes will be connected (`mgmt` in the example below) and the IP addresses from that network to be assigned to each machine (in the example below, `192.168.0.X`). Create `config.yaml`as follows:

```yaml
---
additionalParamsForVnf:
  -
    member-vnf-index: k8s_juju
    additionalParams:
        MACHINE1: "192.168.0.161"
        MACHINE2: "192.168.0.162"
        MACHINE3: "192.168.0.163"
        MACHINE4: "192.168.0.164"
        MACHINE5: ""
        MACHINE6: ""
        MACHINE7: ""
        MACHINE8: ""
        MACHINE9: ""
        MACHINE10: ""
        BUNDLE: ""
vld:
  -
    name: mgmtnet
    vim-network-name: mgmt              #The network in the VIM to connect all nodes of the clusters
    vnfd-connection-point-ref:
      -
        ip-address: "192.168.0.161"
        member-vnf-index-ref: k8s_vnf1
        vnfd-connection-point-ref: mgmt
      -
        ip-address: "192.168.0.162"
        member-vnf-index-ref: k8s_vnf2
        vnfd-connection-point-ref: mgmt
      -
        ip-address: "192.168.0.163"
        member-vnf-index-ref: k8s_vnf3
        vnfd-connection-point-ref: mgmt
      -
        ip-address: "192.168.0.164"
        member-vnf-index-ref: k8s_vnf4
        vnfd-connection-point-ref: mgmt
      -
        ip-address: "192.168.0.170"
        member-vnf-index-ref: k8s_juju
        vnfd-connection-point-ref: mgmt
```

Then, instantiate the NS

```bash
osm ns-create --ns_name k8s-cluster --nsd_name k8s_juju --vim_account <VIM_ACCOUNT> --config_file config.yaml --ssh_keys ${HOME}/.ssh/id_rsa.pub
```

# Check K8s cluster

Connect to the machine running juju (`k8s_juju` in the NS), check that kubeconfig file exists and test kubectl:

```bash
osm vnf-list --ns k8s-cluster --filter vnfd-ref=k8s_jujucontroller_vnf
ssh ubuntu@<JUJU_CONTROLLER_IP_ADDRESS>
cat .kube/config
kubectl get all
```

To access the K8s dashboard:

```bash
juju config kubernetes-master enable-dashboard-addons=true
```

The dashboard could be accesible running this kubectl command:

```bash
kubectl proxy --address <JUJU_CONTROLLER_IP_ADDRESS>
```

And now a SSH tunnel could be done from the computer to the juju controller, and from the web browser access to the dashboard:

<http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/>

