from apt.progress.base import InstallProgress

from utils import (
    install_apt,
)


class Progress(InstallProgress):

    def status_change(self, pkg, percent, status):
        print("status change\n")
        #print(str(int(percent)) + "% \n")
        True


if __name__ == "__main__":
    install_apt(packages=["mate-backgrounds"], update=True, progress=Progress())
