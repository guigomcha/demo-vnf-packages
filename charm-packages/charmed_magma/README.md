# Charmed Magma

This NS allows you to deploy either the Magma AGW VNF only, or the Magma AGW together with the Magma Orchestrator.
Magma AGW is deployed in a VM with a Native Charm while Magma Orchestrator is deployed in a K8s cluster with a Juju bundle.

## Upload the VNF packages

Following commands should be executed in the following folder: `osm-packages/charm-packages/charmed_magma`

### Magma Access GW VNF Package

```bash
osm nfpkg-create magma_agw_vnf/
```

### Magma Orchestrator VNF Package

```bash
osm nfpkg-create magma_orc_cnf/
```

## Upload the NS package

### Magma Access GW only

```bash
osm nspkg-create magma_agw_ns/
```

### Magma Access GW and Magma Orchestrator together

```bash
osm nspkg-create magma_ns/
```

## Configuration

The configuration file `params.yaml` need to be set in order to deploy the Access GW. This needs to be done no matter if you want to deploy the Access GW only or the Access GW and the Orchestrator.

Here is an example:

```yaml
vld: 
  - name: mgmtnet                                # Id of the virtual-link-desc where the SGI interface is
    vim-network-name: osm-ext                    # Name of the network in the VIM (The network must exists)
    vnfd-connection-point-ref: 
      - member-vnf-index-ref: "vnf1"             # constituent-base-element-id (In the NSD)
        vnfd-connection-point-ref: vnf-mgmt-ext  # constituent-cpd-id (In the NSD)
        ip-address: "172.21.248.153"             # IP address that will be used in the mgmt interface in the VM
additionalParamsForVnf: 
  - member-vnf-index: "vnf1"                     # constituent-base-element-id (In the NSD)
    additionalParams: 
      - sgi_ipv4_address: "172.21.248.153/22"    # IP address and subnet that will be used in the mgmt interface in the VM, the format x.x.x.x/yy
        sgi_interface: ens3                      # Name of the SGI interface in the VM
        sgi_ipv4_gateway: "172.21.248.1"         # GW of the network used for SGI
        s1_interface: ens4                       # Name of the S1 interface in the VM
```

## Instantiation

### Magma Access GW only


```bash
osm ns-create --ns_name magma-agw-ns --nsd_name magma-agw-ns --vim_account <VIM_ACCOUNT> --config_file params.yaml
```

### Magma Access GW and Magma Orchestrator together

```bash
osm ns-create --ns_name magma-ns --nsd_name magma-ns --vim_account <VIM_ACCOUNT> --config_file params.yaml
```