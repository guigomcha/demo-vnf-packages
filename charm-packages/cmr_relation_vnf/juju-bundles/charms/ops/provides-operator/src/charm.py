#!/usr/bin/env python3
# Copyright 2021 David Garcia
# See LICENSE file for licensing details.
import logging

from ops.charm import CharmBase
from ops.main import main
from ops.model import ActiveStatus

logger = logging.getLogger(__name__)


class ProvidesCharm(CharmBase):
    """Charm the service."""

    def __init__(self, *args):
        super().__init__(*args)
        self.framework.observe(self.on.install, self._on_install)
        self.framework.observe(
            self.on.interface_relation_changed, self.on_interface_relation_changed
        )

    def _on_install(self, event):
        self.unit.status = ActiveStatus()

    def on_interface_relation_changed(self, event):
        parameter = "Hello"
        event.relation.data[self.model.unit]["parameter"] = parameter
        self.unit.status = ActiveStatus("Parameter sent: {}".format(parameter))


if __name__ == "__main__":
    main(ProvidesCharm)
