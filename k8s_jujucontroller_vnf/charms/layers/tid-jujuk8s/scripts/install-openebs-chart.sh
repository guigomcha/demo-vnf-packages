#!/bin/bash
/snap/bin/helm install --namespace openebs --name openebs stable/openebs
/snap/bin/kubectl apply -f /home/ubuntu/openebs-storage-class.yaml
/snap/bin/kubectl patch storageclass openebs-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'

