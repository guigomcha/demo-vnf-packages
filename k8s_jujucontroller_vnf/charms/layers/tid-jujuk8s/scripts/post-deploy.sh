#!/bin/bash
# Setup kubectl
mkdir -p ~/.kube
/snap/bin/juju scp kubernetes-master/0:config ~/.kube/config
sudo snap install kubectl --classic
# Allow privileges
/snap/bin/juju config kubernetes-master allow-privileged=true

