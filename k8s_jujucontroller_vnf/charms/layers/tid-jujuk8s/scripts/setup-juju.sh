#!/bin/bash
timeout=0
while [ $timeout -lt 900 ]; do
  if [ -f /home/ubuntu/tmp-cloudinit-installed.txt ]; then
      break
  fi
  sleep 10
  timeout=$[$timeout + 10]
done
if [ timeout -eq 900 ]; then
  >&2 echo "Cloudinit took longer than expected (> 900s). Not possible to start setup"
  exit 1
fi
cat /dev/zero | ssh-keygen -q -N ""
MGMT_IP=$(ip -4 -o a s ens3 | awk '{split($4,a,"/"); print a[1]}')
sshpass -posm4u ssh-copy-id -o "StrictHostKeyChecking no" ubuntu@${MGMT_IP}
/snap/bin/juju add-cloud --local k8s -f k8s-cloud.yaml
ssh-copy-id -i /home/ubuntu/.local/share/juju/ssh/juju_id_rsa ubuntu@${MGMT_IP}
/snap/bin/juju bootstrap k8s jujuk8s

