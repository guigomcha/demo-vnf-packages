#!/bin/bash
sshpass -posm4u ssh-copy-id -o "StrictHostKeyChecking no" -i /home/ubuntu/.local/share/juju/ssh/juju_id_rsa ubuntu@$1
sudo su -l ubuntu -c "nohup /snap/bin/juju add-machine ssh:ubuntu@"$1" > /dev/null 2>&1 &"

