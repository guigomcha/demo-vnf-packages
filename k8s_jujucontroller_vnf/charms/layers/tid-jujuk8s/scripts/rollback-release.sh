#!/bin/bash
RELEASE=$1
REVISION="0"
if [ $# -gt 1 ]; then
    REVISION=$2
fi
helm rollback ${RELEASE} ${REVISION}

