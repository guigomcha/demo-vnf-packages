#!/bin/bash
REPO_NAME=$1
REPO_URL=$2
helm repo add ${REPO_NAME} ${REPO_URL}
helm repo update

