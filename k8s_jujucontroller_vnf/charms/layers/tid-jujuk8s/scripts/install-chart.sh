#!/bin/bash
CHART=$1
VALUES_FILE=$2
ADDITIONAL_OPTS=""
if [ $# -gt 2 ]; then
    ADDITIONAL_OPTS="-n $3"
fi
helm install ${CHART} -f ${VALUES_FILE} --atomic ${ADDITIONAL_OPTS}

