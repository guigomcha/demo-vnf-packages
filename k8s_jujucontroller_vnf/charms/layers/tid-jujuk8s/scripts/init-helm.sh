#!/bin/bash
curl https://get.helm.sh/helm-v2.15.2-linux-amd64.tar.gz --output helm-v2.15.2.tar.gz
tar -zxvf helm-v2.15.2.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm
rm -rf linux-amd64/
rm -f helm-v2.15.2.tar.gz
/usr/local/bin/helm init
# Snap for helm currently uses version 3 of helm
#sudo snap install helm --classic
#/snap/bin/helm init

