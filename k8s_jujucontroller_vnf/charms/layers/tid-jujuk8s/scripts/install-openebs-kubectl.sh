#!/bin/bash
/snap/bin/kubectl apply -f https://openebs.github.io/charts/openebs-operator.yaml
/snap/bin/kubectl apply -f /home/ubuntu/openebs-storage-class.yaml
/snap/bin/kubectl patch storageclass openebs-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'

