#!/bin/bash
RELEASE=$1
CHART=$2
ADDITIONAL_OPTS="--atomic"
if [ $# -gt 2 ]; then
    ADDITIONAL_OPTS="${ADDITIONAL_OPTS} -n $3"
fi
if [ $# -gt 3 ]; then
    ADDITIONAL_OPTS="${ADDITIONAL_OPTS} -f $4"
fi

helm upgrade ${RELEASE} ${CHART} ${ADDITIONAL_OPTS}

